## Status
[![pipeline status](https://gitlab.com/bastillebsd-templates/nginx/badges/master/pipeline.svg)](https://gitlab.com/bastillebsd-templates/nginx/commits/master)

## nginx
Bastille template to bootstrap nginx

## Bootstrap

```shell
bastille bootstrap https://gitlab.com/bastillebsd-templates/nginx
```

## Usage

```shell
bastille template TARGET bastillebsd-templates/nginx

```

## TIPS

You will probably call your nginx container "nginx", in which case, you can use this command. 

```shell
bastille template nginx bastillebsd-templates/nginx

```

You will probably want to redirect host ports 80 (http) and 443 (https)  to your nginx container. 

```
bastille rdr TARGET tcp 80 80
bastille rdr TARGET tcp 443 443

```
or if your container is called nginx

```
bastille rdr nginx tcp 80 80
bastille rdr nginx tcp 443 443

```

If you want to use the lets encrypt certbot you should use one of the following commands

```
bastille pkg TARGET install py38-certbot-nginx
bastille pkg TARGET install py39-certbot-nginx
bastille pkg nginx  install py38-certbot-nginx
bastille pkg nginx  install py39-certbot-nginx


```

for testing I like to use wget and siege
```
bastille pkg TARGET install wget siege
bastille pkg nginx  install wget siege

```

And when I am in a terminal, in a BSD vps, in a Bastille Container, trying to configure nginx,  if I want to browse the the web site I am trying to reverse proxy behind nginx, I use the ascii browser lynx. 

````
bastille pkg TARGET install lynx
bastille pkg nginx install lynx


```

remember to make sure that your pf configuration file /etc/pf.conf allows tcp into ports 40 and 443. 

Have fun!
